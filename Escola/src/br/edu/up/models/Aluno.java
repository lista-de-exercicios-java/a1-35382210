package br.edu.up.models;

public class Aluno {

    private int matricola;
    private String nome;
    private int nota;

    public Aluno(int matricola, String nome, int nota) {
        this.matricola = matricola;
        this.nome = nome;
        this.nota = nota;
    }

    public Aluno() {
    }

    public int getMatricola() {
        return matricola;
    }

    public void setMatricola(int matricola) {
        this.matricola = matricola;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }

    public String toCSV(){
        return matricola + ";" + nome + ";" + nota ;
    }

}
