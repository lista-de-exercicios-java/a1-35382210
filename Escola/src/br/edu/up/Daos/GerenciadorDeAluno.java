package br.edu.up.Daos;

    import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import br.edu.up.models.Aluno;


public class GerenciadorDeAluno {

    


    public GerenciadorDeAluno() {
        
    }

    private String header = "matricola;nome;nota";

    private String nomeDoArquivo = "src/br/edu/up/Daos/csv/alunos.csv";

    List<Aluno> listaDeAluno = new ArrayList<>();

    public List<Aluno> getAluno() {

        try {

            File arquivoLeitura = new File(nomeDoArquivo);

            Scanner leitor = new Scanner(arquivoLeitura);

            if (leitor.hasNextLine()) {
                header = leitor.nextLine();
            }

            while (leitor.hasNextLine()) {
                String linha = leitor.nextLine();
                String[] dados = linha.split(";");


                int matricola = Integer.parseInt(dados[0]);
                String nome = dados[1];
                int nota = Integer.parseInt(dados[2]);

                Aluno aluno = new Aluno(matricola,nome,nota);
                listaDeAluno.add(aluno);
            }
            leitor.close();

        } catch(Exception e){
            System.out.println("Arquivo não encontrado!");
        }

        return  listaDeAluno;

    }

    public void gravarArquivo() {

        try {

            FileWriter arquivoGravar = new FileWriter(nomeDoArquivo);
            PrintWriter gravador = new PrintWriter(arquivoGravar);
            gravador.println(header);

            for (Aluno aluno : listaDeAluno) {
                gravador.println(aluno.toCSV());
            }

            gravador.close();

        } catch (IOException e) {
            System.out.println("Não foi possível gravar o arquivo!");
        }
    }
    
}


